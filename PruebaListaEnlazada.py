'''
Created on 12/10/2021

@author: Herrera
'''

class Nodo:
    
    def __init__(self, dato):
        self.dato = dato
        self.nodoSiguiente = None
    
    def __str__(self):
        return str(self.value)
    
class ListaEnlazada:
    def __init__(self):
        self.nodoInicio = None
        self.nodoFin = None
        
    def verificarListaVacia(self):
        if (self.nodoInicio == None):
            return True
        else:
            return False
    
    def agregarElementoInicio(self, dato):
        nodoNuevo = Nodo(dato)
        if(self.nodoInicio == None):
            self.nodoInicio = self.nodoFin = nodoNuevo
            print("\nSe agrego el dato")
        else:
            nodoNuevo.nodoSiguiente = self.nodoInicio
            self.nodoInicio = nodoNuevo
            print("\nSe agrego el dato")
    
    def agregarAlFinal(self, dato):
        nodoNuevo = Nodo(dato)
        if(self.nodoInicio == None):
            self.nodoInicio = self.nodoFin = nodoNuevo
            print("\nSe agrego el dato")
        else:
            self.nodoFin.nodoSiguiente = nodoNuevo
            self.nodoFin = nodoNuevo
            print("\nSe agrego el dato")
    
    def agregarElementoPosicionEspecifica(self, posicion, dato):
        nodoNuevo = Nodo(dato)
        if(posicion == 0):
            ListaEnlazada.agregarElementoInicio(self, dato)
        elif(self.nodoInicio == None):
            print("\nNo se encontro la pocicion")
        else:
            nodoActual = self.nodoInicio.nodoSiguiente
            nodoAnterior = self.nodoInicio
            cont = 1
            
            while(nodoActual != None and posicion != cont):
                cont+=1
                nodoActual = nodoActual.nodoSiguiente
                nodoAnterior = nodoAnterior.nodoSiguiente
            if(cont == posicion):
                nodoAnterior.nodoSiguiente = nodoNuevo
                nodoNuevo.nodoSiguiente = nodoActual   
                print("\nSe agrego el dato")
            else:
                print("\nNo se encontro la pocicion")
            
        
    def eliminarInicio(self):
        if(self.nodoInicio == None):
            print("\nLista vacia")
        else:
            if(self.nodoInicio == self.nodoFin):
                self.nodoFin = self.nodoInicio = None
                print("\nSe elimino el dato")
            else:
                self.nodoInicio = self.nodoInicio.nodoSiguiente
                print("\nSe elimino el dato")
    
    def eliminarFinal(self):
        if(self.nodoInicio == None):
            print("\nLista vacia")
        else:
            if(self.nodoInicio == self.nodoFin):
                self.nodoInicio = self.nodoFin = None
                print("\nSe elimino el dato")
            else:
                nodoActual = self.nodoInicio
                nodoAnterior = None
                
                while(nodoActual != self.nodoFin):
                    nodoAnterior = nodoActual
                    nodoActual = nodoActual.nodoSiguiente
                
                nodoAnterior.nodoSiguiente = None
                self.nodoFin = nodoAnterior
                print("\nSe elimino el dato")
                
    def eliminarElementoPosicionEspecifica(self, posicion):
        if(self.nodoInicio == None):
            print("\nLista vacia")
        elif (posicion == 0):
            ListaEnlazada.eliminarInicio(self)
        else:
            cont = 1
            nodoActual = self.nodoInicio.nodoSiguiente
            nodoAnterior = self.nodoInicio
            while(nodoActual.nodoSiguiente != None and cont != posicion):
                cont += 1
                nodoAnterior = nodoAnterior.nodoSiguiente
                nodoActual = nodoActual.nodoSiguiente 
            if(cont == posicion):
                nodoAnterior.nodoSiguiente = nodoActual.nodoSiguiente
                print("\nDato eliminado");
            else:
                print("\nNo se encontro la posicion");
    
    def eliminarEspecifico(self, dato):
        if(self.nodoInicio == None):
            print("\nLista vacia")
        elif (self.nodoInicio.dato == dato):
            ListaEnlazada.eliminarInicio(self)
        else:
            nodoAnterior = self.nodoInicio
            nodoActual = self.nodoInicio.nodoSiguiente
            while(dato != nodoActual.dato and nodoActual.nodoSiguiente != None):
                nodoAnterior = nodoAnterior.nodoSiguiente
                nodoActual = nodoActual.nodoSiguiente
            
            if(nodoActual.dato == dato):
                nodoAnterior.nodoSiguiente = nodoActual.nodoSiguiente
                print("\nSe elimino el dato")
            else:
                print("\nNo se encontro el dato")
            
    def mostrarCantidadElementos(self):
        cont = 0
        nodoActual = self.nodoInicio
        
        while(nodoActual != None):
            cont += 1
            nodoActual = nodoActual.nodoSiguiente
            
        print(f"\nCantidad de elementos: {cont}")
        
    def mostrarListaEnlazada(self):
        nodoActual = self.nodoInicio
        print()
        while(nodoActual != None):
            print(f"[{nodoActual.dato}] --> ", end = " ")
            nodoActual = nodoActual.nodoSiguiente  
        print()  
        
opcion = 0

le1 = ListaEnlazada()
while(opcion != 10):
    print("\nElige opcion")
    print("1) Agregar elemento inicio")
    print("2) Agregar elemento Final")
    print("3) Eliminar elemento inicio")
    print("4) Eliminar elemento final")
    print("5) Mostrar datos")
    print("6) Mostrar cantidad de elementos")
    print("7) eliminar elemento en posicion especifica")
    print("8) agregar elemento en posicion especifica")
    print("9) eliminar dato")
    print("10) Salir")
    opcion = int(input("Introduce opcion: "))
    
    if(opcion == 1):
        dato = int(input("Introduce dato: "))
        le1.agregarElementoInicio(dato)
    elif(opcion == 2):
        dato = int(input("Introduce dato: "))
        le1.agregarAlFinal(dato)
    elif(opcion == 3):
        le1.eliminarInicio()
    elif(opcion == 4):
        le1.eliminarFinal()
    elif(opcion == 5):
        le1.mostrarListaEnlazada()
    elif(opcion == 6):
        le1.mostrarCantidadElementos()
    elif(opcion == 7):
        posicion = int(input("Introduce posicion: "))
        le1.eliminarElementoPosicionEspecifica(posicion)
    elif(opcion == 8):
        posicion = int(input("Introduce posicion: "))
        dato = int(input("Introduce dato: "))
        le1.agregarElementoPosicionEspecifica(posicion, dato)
    elif(opcion == 9):
        dato = int(input("Introduce dato: "))
        le1.eliminarEspecifico(dato)
    elif(opcion == 10):
        print("Saliendo , . .")
    
    